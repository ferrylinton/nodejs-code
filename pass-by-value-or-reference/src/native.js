function main(){
    var name    = 'Ferry';  // nilai awal
    var num     = 10;       // nilai awal
    var status  = true;     // nilai awal 

    console.log('### sebelum fungsi modify()');
    print(name, num, status);
    
    modify(name, num, status);

    console.log('### sesudah fungsi modify()');
    print(name, num, status);
}

function modify(name, num, status) {
    name    = 'modified'; // nilai di-copy dan diubah
    num     = 11;         // nilai di-copy dan diubah
    status  = false;      // nilai di-copy dan diubah

    console.log('### di dalam fungsi modify()');
    print(name, num, status)
}

function print(name, num, status){
    console.log('name    : ' + name);
    console.log('num     : ' + num);
    console.log('status  : ' + status);
    console.log('\n');
}

main();