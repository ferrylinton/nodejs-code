function main() {
    var data = {
        name: 'Ferry',
        num: 10,
        status: true,
    }

    console.log('### sebelum fungsi modify()');
    console.log(data);
    console.log('\n');

    modify(data);

    console.log('### sesudah fungsi modify()');
    console.log(data);
    console.log('\n');

    modifyField(data);

    console.log('### sesudah fungsi modifyField()');
    console.log(data);
    console.log('\n');
}

function modify(data) {
    data = {
        name: 'modified',
        num: 11,
        status: false
    }

    console.log('### di dalam fungsi modify()');
    console.log(data);
    console.log('\n');
}

function modifyField(data) {
    data.name = 'modified';
    data.num = 11;
    data.status = false;

    console.log('### di dalam fungsi modifyField()');
    console.log(data);
    console.log('\n');
}

main();