# Javascript **Pass By Value** atau **Pass By Reference** ?

Pada saat membuat aplikasi, kita akan menggunakan banyak fungsi, dan fungsi yang dipanggil biasanya menggunakan parameter. **Pass By Value** atau **Pass By Reference** merupakan istilah yang sering digunakan pada saat mengirim parameter di dalam fungsi.

Secara sederhana, **Pass By Value** artinya nilai parameter di-copy, jadi parameter setelah fungsi dipanggil tidak mempengaruhi nilai parameter sebelum fungsi dipanggil.
Sedangkan **Pass By Reference** artinya alamat memori parameter yang dikirim,


## Contoh Dengan Nilai Primitif 

### Kode

```
function main(){
    var name    = 'Ferry';  // nilai awal
    var num     = 10;       // nilai awal
    var status  = true;     // nilai awal 

    console.log('### sebelum fungsi modify()');
    print(name, num, status);
    
    modify(name, num, status);

    console.log('### sesudah fungsi modify()');
    print(name, num, status);
}

function modify(name, num, status) {
    name    = 'modified'; // nilai di-copy dan diubah
    num     = 11;         // nilai di-copy dan diubah
    status  = false;      // nilai di-copy dan diubah

    console.log('### di dalam fungsi modify()');
    print(name, num, status)
}

function print(name, num, status){
    console.log('name    : ' + name);
    console.log('num     : ' + num);
    console.log('status  : ' + status);
    console.log('\n');
}

main();
```

### Hasil

```
### sebelum fungsi modify()
name    : Ferry
num     : 10
status  : true


### di dalam fungsi modify()
name    : modified
num     : 11
status  : false


### sesudah fungsi modify()
name    : Ferry
num     : 10
status  : true
```

Dari hasil bisa disimpulkan, parameter dengan nilai primitif, javascript menggunakan **Pass By Value**. Karena nilai parameter (name, num, status) di dalam fungsi primitif tidak berubah pada saat fungsi modify() dipanggil. 

## Contoh Dengan objek

### Kode

```
function main() {
    var data = {
        name: 'Ferry',
        num: 10,
        status: true,
    }

    console.log('### sebelum fungsi modify()');
    console.log(data);
    console.log('\n');

    modify(data);

    console.log('### sesudah fungsi modify()');
    console.log(data);
    console.log('\n');

    modifyField(data);

    console.log('### sesudah fungsi modifyField()');
    console.log(data);
    console.log('\n');
}

function modify(data) {
    data = {
        name: 'modified',
        num: 11,
        status: false
    }

    console.log('### di dalam fungsi modify()');
    console.log(data);
    console.log('\n');
}

function modifyField(data) {
    data.name = 'modified';
    data.num = 11;
    data.status = false;

    console.log('### di dalam fungsi modifyField()');
    console.log(data);
    console.log('\n');
}

main();
```

### Hasil

```
### sebelum fungsi modify()
{ name: 'Ferry', num: 10, status: true }


### di dalam fungsi modify()
{ name: 'modified', num: 11, status: false }


### sesudah fungsi modify()
{ name: 'Ferry', num: 10, status: true }


### di dalam fungsi modifyField()
{ name: 'modified', num: 11, status: false }


### sesudah fungsi modifyField()
{ name: 'modified', num: 11, status: false }

```

Dari hasil bisa dilihat, kalau objek di-assign dengan objek baru, javascript menggunakan **Pass By Value**. Tapi kalau fungsi merubah properti objek, parameter sebelum fungsi akan berubah, jadi mirip seperti **Pass By Reference**.

Meskipun hasilnya bisa membuat kita bingung, javascript menggunakan **Pass By Value**, dibuktikan dengan kondisi yang pertama.

