require('dotenv').config({ path: process.cwd() + '/.env.test', debug: true});
const assert = require('assert');
const messageService = require('../src/service/message-service');

describe('messageService', function () {
    describe('#getMessage()', function () {

        it('should return "This is message from test"', function () {
            let result = messageService.getMessage();
            assert.strictEqual(result, "This is message from test");
        });

    });
});