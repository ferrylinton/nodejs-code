const express = require('express')
const handlebarsConfig = require('./config/handlebars-config');

const port = 3000;
const app = express();
handlebarsConfig(app);


app.get('/', (req, res, next) => {
  let data = {
    date : new Date()
  }
  res.render('home', data);
})

app.get('/about', (req, res, next) => {
  let data = {
    msg : 'Horas !!'
  }
  res.render('about', data);
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})