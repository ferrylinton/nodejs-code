<img src="../images/favicon.png" width="100px" height="100px" style="margin: 0 0 15px 0"/>

# bologhu

## ExpressJS Dengan Nodemon

Nodemon merupakan alat bantu pada saat mengembangkan aplikasi NodeJS. Setiap melakukan perubahan di dalam kode, kita harus memuat ulang aplikasi. Dengan Nodemon, perubahan yang ada akan dimuat ulang secara otomatis. 

## Lihat posting

[Menjalankan ExpressJS Dengan Nodemon](https://bologhu.com/posts/expressjs-nodemon)