const assert = require('assert');
const calculator = require('../src/calculator');

describe('calculator', function () {
    describe('#add()', function () {

        it('(1 + 2) should return 3', function () {
            let result = calculator.add(1, 2);
            assert.strictEqual(result, (1 + 2));
        });

        it('(1 + 2) should return 3 not 12', function () {
            let result = calculator.add(1, 2);
            assert.notStrictEqual(result, 12);
        });

    });
});