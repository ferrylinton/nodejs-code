<img src="../images/favicon.png" width="100px" height="100px" style="margin: 0 0 15px 0"/>

# bologhu

## Menjalankan Hello World Dengan NodeJS

Membuat program NodeJS yang sederhana, untuk menjalankan kode sederhana. Projek tidak menggunakan library NodeJS yang lain, kode hanya akan menampilankan teks di terminal.

## Lihat posting

[Menjalankan Hello World Dengan NodeJS](https://bologhu.com/posts/nodejs-hello-world)

