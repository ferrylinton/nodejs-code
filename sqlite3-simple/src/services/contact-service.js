const db = require('./sqlite-service');


const FIND = `SELECT * FROM contact`;
const FIND_BY_ID = `SELECT * FROM contact WHERE id = ?`;
const INSERT = `INSERT INTO contact(name) VALUES(?)`;
const UPDATE = `UPDATE contact set name = ?, enabled = ? where id = ?`;
const DESTROY = `DELETE FROM contact WHERE id = ?`;
const TRUNCATE = `DELETE FROM contact`;

function find(params) {
    return new Promise((resolve, reject) => {
        db.all(FIND, params, function(err, rows){
            if (err) {
                console.log(err);
                reject(err);
            } else {
                resolve(rows);
            }
        });
    })
}

function findById(id) {
    return new Promise((resolve, reject) => {
        db.get(FIND_BY_ID, [id], function(err, row){
            if (err) {
                console.log(err);
                reject(err);
            } else {
                resolve(row);
            }
        });
    })
}

function create(name) {
    return new Promise((resolve, reject) => {
        db.run(INSERT, [name], function(err){
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log(INSERT);
                console.log(this);
                resolve(this);
            }
        });
    })
}

function update(contact) {
    return new Promise((resolve, reject) => {
        db.run(UPDATE, [contact.name, contact.enabled, contact.id], function(err){
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log(this);
                resolve(this);
            }
        });
    })
}

function destroy(id) {
    return new Promise((resolve, reject) => {
        db.run(DESTROY, [id], function(err){
            if (err) {
                console.log(err);
                reject(err);
            } else {
                resolve(this);
            }
        });
    })
}

function truncate() {
    return new Promise((resolve, reject) => {
        db.run(TRUNCATE, [], function(err){
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log(`truncate table 'contact'`);
                console.log(this);
                resolve(this);
            }
        });
    })
}


module.exports = {
    find,
    findById,
    create,
    update,
    destroy,
    truncate
}