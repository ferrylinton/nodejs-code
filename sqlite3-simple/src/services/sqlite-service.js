const sqlite3 = require('sqlite3').verbose();

const CREATE_TABLE_CONTACT = `CREATE TABLE IF NOT EXISTS contact (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL UNIQUE,
	enabled INTEGER NOT NULL DEFAULT 0
)`

function createConnection() {
    console.log('start connecting to DB ...');
    return new sqlite3.Database(process.env.FILENAME,
        sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,
        (err) => {
            if (err) {
                console.error(err);
                throw err;
            } else {
                console.log(`Connected to ${process.env.FILENAME}`);
            }
        });
}
function createTables(db) {
    console.log('start creating tables ...');
    db.run(CREATE_TABLE_CONTACT, (err) => {
        if (err) {
            console.error(err);
            throw err;
        } else {
            console.log(`tables is created`);
        }
    });
}

let db = createConnection();
createTables(db);

module.exports = db;