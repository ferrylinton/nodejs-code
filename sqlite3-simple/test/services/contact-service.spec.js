require('dotenv').config({ path: process.cwd() + '/.env.test', debug: true });
const assert = require('assert');
const contactService = require('../../src/services/contact-service');

describe('Contact', function () {

    before((done) => {
        (async () => {
            await contactService.truncate();
            done();
        })()
    });

    describe('#create()', async function () {

        var id = 0;

        it(`should create 1 contact`, async function () {
            let result = await contactService.create('Ferry');
            id = result.lastID;
            assert.strictEqual(result.changes, 1);
        });

        it(`should return 1 contact`, async function () {
            let rows = await contactService.find();
            assert.strictEqual(rows.length, 1);
        });

        it(`should return contact with id`, async function () {
            let contact = await contactService.findById(id);
            assert.strictEqual(contact.id, id);
        });

        it(`should update contact`, async function () {
            let contact = await contactService.findById(id);
            contact.name = 'Ferry xxx';
            contact.enabled = 1;

            await contactService.update(contact);
            contact = await contactService.findById(id);
            assert.strictEqual(contact.name, 'Ferry xxx');
        });

        it(`should delete contact`, async function () {
            await contactService.destroy(id);
            let contact = await contactService.findById(id);
            assert.strictEqual(contact, undefined);
        });

    });
});