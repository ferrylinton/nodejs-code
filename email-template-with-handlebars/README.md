# Email Template Dengan Handlebars

Membuat program NodeJS yang sederhana, untuk menjalankan kode sederhana. Projek tidak menggunakan library NodeJS yang lain, kode hanya akan menampilankan teks di terminal.

## Library

1. [Handlebars](https://handlebarsjs.com/)


## Langkah-Langkah Yang Digunakan

### 1. Buat Projek NodeJS

Struktur projek yang akan digunakan:

```
.
├── package.json
├── package-lock.json
├── README.md
├── src
│   ├── index.js
│   ├── services
│   │   └── template-service.js
│   └── templates
│       └── email.hbs
└── temp
    └── email.html

```

### 2. Install Handlebars

```
npm install handlebars
```

### 3. Buat file template-service.js, dan tulis kode berikut

```
const fs = require('fs');
const path = require('path');
const Handlebars = require('handlebars');

function getHtml(filename, data) {
    let source = fs.readFileSync(path.join(process.cwd(), 'src', 'templates', filename), 'utf8');
    let template = Handlebars.compile(source);
    return template(data);
}

module.exports = {
    getHtml
};
```

### 4. Buat file email.hbs, dan tulis kode berikut

```
<!DOCTYPE html>
<html lang="id">

<head>
    <title>bologhu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {
            background-color: #ffffff;
            font-family: sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
            line-height: 1.4;
            margin: 10px;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            min-width: 360px;
        }


        .btn-confirm {
            margin: 0 auto;
            display: inline-block;
            color: #ffffff;
            text-decoration: none;
            text-align: center;
            font-weight: bold;
            font-size: 20px;
            padding: 4px 10px;
            text-transform: uppercase;
            background-color: #595a62;
        }

        .btn-confirm:hover {
            background-color: #54555a;
        }
    </style>
</head>

<body>
    <div style="margin: 0 0 15px 0;">Hi <strong>{{username}}</strong>,</div>

    <div style="margin: 0 0 15px 0;">
        Terima kasih sudah mendaftar di bologhu.com. Klik tombol dibawah untuk mengkonfirmasi data anda.
    </div>
    <div style="margin: 0 auto;">
        <a class="btn-confirm" href="{{url}}">Click this link</a>
    </div>

    <div style="margin: 15px 0;">Kind Regards,</div>
    <div><strong>admin@bologhu.com</strong></div>
</body>

</html>
```

### 5. Buat file index.js, dan tulis kode berikut

```
const fs = require('fs');
const path = require('path');
const templateService = require('./services/template-service');

create();

function create() {
    try {
        let file = path.join(process.cwd(), 'temp', 'email.html');
        let data = {
            username: 'ferrylinton',
            url: 'www.bologhu.com'
        };

        let html = templateService.getHtml('email.hbs', data);
        fs.writeFileSync(file, html);
    } catch (err) {
        console.error(err)
    }
}
```

### 6. Jalankan index.js

```
node src/index.js
```

### 7. Hasil ada di **temp.email.html**

```
<!DOCTYPE html>
<html lang="id">

<head>
    <title>bologhu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {
            background-color: #ffffff;
            font-family: sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
            line-height: 1.4;
            margin: 10px;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            min-width: 360px;
        }


        .btn-confirm {
            margin: 0 auto;
            display: inline-block;
            color: #ffffff;
            text-decoration: none;
            text-align: center;
            font-weight: bold;
            font-size: 20px;
            padding: 4px 10px;
            text-transform: uppercase;
            background-color: #595a62;
        }

        .btn-confirm:hover {
            background-color: #54555a;
        }
    </style>
</head>

<body>
    <div style="margin: 0 0 15px 0;">Hi <strong>ferrylinton</strong>,</div>

    <div style="margin: 0 0 15px 0;">
        Terima kasih sudah mendaftar di bologhu.com. Klik tombol dibawah untuk mengkonfirmasi data anda.
    </div>
    <div style="margin: 0 auto;">
        <a class="btn-confirm" href="www.bologhu.com">Click this link</a>
    </div>

    <div style="margin: 15px 0;">Kind Regards,</div>
    <div><strong>admin@bologhu.com</strong></div>
</body>

</html>
```