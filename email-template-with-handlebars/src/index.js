const fs = require('fs');
const path = require('path');
const templateService = require('./services/template-service');

create();

function create() {
    try {
        let file = path.join(process.cwd(), 'temp', 'email.html');
        let data = {
            username: 'ferrylinton',
            url: 'www.bologhu.com'
        };

        let html = templateService.getHtml('email.hbs', data);
        fs.writeFileSync(file, html);
    } catch (err) {
        console.error(err)
    }
}
