const assert = require('assert');
const { Contact } = require('../../src/models/index');

describe('Contact', function () {

    describe('#find()', async function () {

        it(`should return 4 contacts`, async function () {
            let contacts = await Contact.findAll();
            assert.strictEqual(contacts.length, 4);
            
            contacts.forEach(function(contact) {
                console.log(contact.toJSON());
            });

        });


    });
});