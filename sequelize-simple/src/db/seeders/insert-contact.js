
module.exports = {
    up: async (queryInterface) => {

        var contacts = [
            { name: 'Superman', enabled: true },
            { name: 'Batman', enabled: true },
            { name: 'Aquaman', enabled: false },
            { name: 'Wonderwoman', enabled: false }
        ];

        await queryInterface.bulkInsert('t_contact', contacts, {});

    },

    down: async (queryInterface) => {

        await queryInterface.bulkDelete('t_contact', null, {});

    }
};

